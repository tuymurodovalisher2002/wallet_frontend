import { Col, Row } from "antd";
import React from "react";

import Header from "../Navbar/Header";
import Navbar from "../Navbar/Navbar";

import style from "./Home.module.scss";

function Home() {
  return (
    <div className={style.home}>
      <Row>
        <Col span={24}>
          <Header />
        </Col>
      </Row>
      <Row>
        <Col span={3}>
          <Navbar />
        </Col>
        <Col span={21}>
          <div className={style.home_content}></div>
        </Col>
      </Row>
    </div>
  );
}

export default Home;
