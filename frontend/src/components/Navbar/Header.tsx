import React from "react";

import LogoIcon from "../../assets/icons/LogoIcon";

import style from "./Sidebar.module.scss";

function Header() {
  return (
    <div className={style.header}>
      <LogoIcon />
    </div>
  );
}

export default React.memo(Header);
