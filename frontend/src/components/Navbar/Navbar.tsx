import React from "react";
import { Link } from "react-router-dom";
import DashboardIcon from "../../assets/icons/DashboardIcon";
import SettingsIcon from "../../assets/icons/SettingsIcon";

import style from "./Sidebar.module.scss";

function Navbar() {
  return (
    <div className={style.navbar}>
      <div className={style.navbar_url}>
        <Link to={"/"}>
          <DashboardIcon />
        </Link>
        <Link to={"/"}>
          <SettingsIcon />
        </Link>
      </div>
    </div>
  );
}

export default React.memo(Navbar);
